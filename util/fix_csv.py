import re

FILENAME_IN = 'deidentification_rules.csv'
FILENAME_OUT = 'deidentification_rules_mod.csv'

def fix_tag_string(tag):
    tag_new = '"(' + ','.join(tag) + ')"'
    return tag_new

def fix_line(line):
    line_new = line
    tag_pattern = '^([\da-fA-F]{4})([\da-fA-F]{4})'
    matches = re.findall(tag_pattern, line)
    for tag in matches:
        tag_new = fix_tag_string(tag)
        line_new = re.sub(''.join(tag), tag_new, line)
    
    return line_new


with open(FILENAME_IN,'r') as f, open(FILENAME_OUT, 'w+') as fout:
    
    # Handle first line (headers) separately
    fout.write( f.readline() )

    # Loop through CSV and update tag format
    [ fout.write( fix_line(line) ) for line in f ]

