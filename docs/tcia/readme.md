# De-identification Rules from TCIA
The Cancer Imaging Archive has a [wiki](https://wiki.cancerimagingarchive.net/display/Public/De-identification+Knowledge+Base) discussing de-identification of DICOM files from which the tables here-in were extracted. 

A similar table was discovered in the current NEMA standard, so this will probably be used instead.
